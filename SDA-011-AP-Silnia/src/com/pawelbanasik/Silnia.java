package com.pawelbanasik;

import java.util.Scanner;

public class Silnia {

	public static void main(String[] args) {

		// iloczyn tablicy
		int[] myArray = { 1, 2, 3, 4, 5 };

		int iloczyn = 1;

		for (int i = 0; i < myArray.length; i++) {

			System.out.println(myArray[i]);

			iloczyn = iloczyn * myArray[i];

		}
		System.out.println("Iloczyn powyzszych: " + iloczyn);

		// Silnia ze skanerem
		Scanner sc = new Scanner(System.in);

		System.out.println("Wpisz liczbe do zrobienia silni: ");

		int n = sc.nextInt();

		int silnia = 1;
		for (int i = 2; i <= n; i++) {
			silnia = silnia * i;
			System.out.println(silnia);

			sc.close();

		}

	}

}
