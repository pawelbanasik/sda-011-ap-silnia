package com.pawelbanasik;

public class SilniaRekurencja {

	public static void main(String[] args) {

		System.out.println(silnia(5));

	}

	private static int silnia(int value) {
		System.out.println(value);
		if (value == 1) {

			return 1;
		}
		return silnia(value - 1) * value;
	}

}
